SetFactory("OpenCASCADE");

//core & winding parameters
straighSectionLength=20.00;
sectionWidth=10.00;
coreHole=5.00;
coreHeight=0.30;

//+
Point(40001) = {-straighSectionLength/2 , sectionWidth/2, -coreHeight/2, 1.0};
//+
Point(40002) = {-straighSectionLength/2 , -sectionWidth/2, -coreHeight/2, 1.0};
//+
Point(40003) = {straighSectionLength/2 , sectionWidth/2, -coreHeight/2, 1.0};
//+
Point(40004) = {straighSectionLength/2 , -sectionWidth/2, -coreHeight/2, 1.0};
//+
Point(40005) = {straighSectionLength/2 , coreHole/2, -coreHeight/2, 1.0};
//+
Point(40006) = {straighSectionLength/2 , -coreHole/2, -coreHeight/2, 1.0};
//+
Point(40007) = {-straighSectionLength/2 , coreHole/2, -coreHeight/2, 1.0};
//+
Point(40008) = {-straighSectionLength/2 , -coreHole/2, -coreHeight/2, 1.0};
//+
Point(40009) = {straighSectionLength/2 , 0, -coreHeight/2, 1.0};
//+
Point(40010) = {-straighSectionLength/2 , 0, -coreHeight/2, 1.0};


//+
Circle(40001) = {40002, 40010, 40001};
//+
Circle(40002) = {40008, 40010, 40007};
//+
Circle(40003) = {40003, 40009, 40004};
//+
Circle(40004) = {40005, 40009, 40006};
//+
Line(40005) = {40004, 40002};
//+
Line(40006) = {40006, 40008};
//+
Line(40007) = {40005, 40007};

Line(40008) = {40003, 40001};

//+
Curve Loop(40001) = {40001, -40008, 40003, 40005};
//+
Curve Loop(40002) = {40002, -40007, 40004, 40006};

//+
Plane Surface(40001) = {40001, 40002};
//+

Extrude {0, 0, coreHeight} {
  Surface{40001}; 
}

Physical Volume("core") = {40001};