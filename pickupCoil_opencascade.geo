SetFactory("OpenCASCADE");


pickupCoil_height=2;
pickupCoil_width=14;
pickupCoil_length=30;
pickupCoil_turns=100;

pickupCoil_wireDia=0.1;
wireCrossectionSelect=1; //set to 2 for circular or to 1 for square cross-section 
rad=0.5; // winding bend radius

// do not change
z=pickupCoil_height; 
y=pickupCoil_width; 
x=0;


turns=pickupCoil_turns;
spacing=pickupCoil_length/turns; 
dia=pickupCoil_wireDia; // wire diameter
Printf("spacing %g",spacing);

offy=-y/2;
offz=-z/2;
offx=-turns*spacing/2;

If (wireCrossectionSelect==1)

Point(50000)={dia/2+offx,rad+offy,dia/2+offz,0.5};
Point(50001)={-dia/2+offx,rad+offy,dia/2+offz,0.5};
Point(50002)={-dia/2+offx,rad+offy,-dia/2+offz,0.5};
Point(50003)={dia/2+offx,rad+offy,-dia/2+offz,0.5};
Line(50000)={50000,50001};
Line(50001)={50001,50002};
Line(50002)={50002,50003};
Line(50003)={50003,50000};
Curve Loop(1) = {50000, 50001, 50002, 50003};
Plane Surface(1) = {1};

Else
Point(50004)={offx,rad+offy,offz,0.5};
Circle(50005) = {offx, rad+offy, offz, dia/2, 0, 2*Pi};
Rotate {{1, 0, 0},  {offx, rad+offy, offz}, Pi/2} {
  Curve{50005}; 
}
Curve Loop(1) = {50005};
Plane Surface(1) = {1};

EndIf


// 3D path for path pipe extrude 
For t In {0:(turns-1)}

Point(12*t+1)={x+offx,rad+offy,0+offz,0.5};
Point(12*t+2)={x+offx,y-rad+offy,+offz,0.5};
Point(12*t+3)={x+offx,y-rad+offy,rad+offz,0.5};
Point(12*t+4)={x+offx,y+offy,rad+offz,0.5};
Point(12*t+5)={x+offx,y+offy,z-rad+offz,0.5};
Point(12*t+6)={x+offx,y+offy-rad,z-rad+offz,0.5};
Point(12*t+7)={x+offx,y+offy-rad,z+offz,0.5};

x += spacing;

Point(12*t+8)={x+offx,0+rad+offy,z+offz,0.5};
Point(12*t+9)={x+offx,0+rad+offy,z-rad+offz,0.5};
Point(12*t+10)={x+offx,0+offy,z-rad+offz,0.5};
Point(12*t+11)={x+offx,0+offy,rad+offz,0.5};
Point(12*t+12)={x+offx,rad+offy,rad+offz,0.5};

Line(8*t+1)={12*t+1,12*t+2};
Circle(8*t+2)={12*t+2,12*t+3,12*t+4};
Line(8*t+3)={12*t+4,12*t+5};
Circle(8*t+4)={12*t+5,12*t+6,12*t+7};
Line(8*t+5)={12*t+7,12*t+8};
Circle(8*t+6)={12*t+8,12*t+9,12*t+10};
Line(8*t+7)={12*t+10,12*t+11};

If (t>=1)
Circle(8*(t-1)+8)={12*(t-1)+11,12*(t-1)+12,12*(t)+1};
EndIf

EndFor

// final point
t=turns;
Point(12*t+1)={x+offx,rad+offy,+offz,0.5};
Circle(8*(t-1)+8)={12*(t-1)+11,12*(t-1)+12,12*(t)+1};

// pipe extrude
Wire(3) = {1:(8*turns)};
Extrude { Surface{1}; } Using Wire {3}

// creates physical surfaces for connection to Inductex ports

Physical Surface("pickupP1", 20002) = {1};

If (wireCrossectionSelect==1)
Physical Surface("pickupP2",20003) = {32*t+3};
Else
Physical Surface("pickupP2",20003) = {8*t+3};
EndIf

Printf("End");