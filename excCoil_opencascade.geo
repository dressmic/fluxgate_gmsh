SetFactory("OpenCASCADE");


/* MATLAB script generated variables for smooth transition of the wire between the individual loops. I had to add the arc curve to the connection of adjacent loops, otherwise it created pipe extrusion that had deformed cross-section as it did not rotate the plane of the extrusion to the normal plane of the wire path */


//core & winding parameters
straighSectionLength=20.00;
sectionWidth=10.00;
coreHole=5.00;
coreHeight=0.30;

wireCrossectionSelect=1; //set to 2 for circular or to 1 for square cross-section 


//straight section params
spacing=0.434783;turns=46;tanrad=1.0;ang1=3.336304;ang2=0.194711;p1x=0.018896;p1y=-0.193483;p2x=-0.018896;p2y=0.193483;
//arc section params
circleTurns=25;shiftRAWX=0.12533;shiftRAWY=0.00789;tanrad=1.0;ang3=3.424036;ang4=0.282443;p3x=0.039623;p3y=-0.278703;p4x=-0.039623;p4y=0.278703;





eps=0.1;
wireDia=0.15; // excitation winding diameter

z=coreHeight+2*wireDia; // coil height

y=(sectionWidth-coreHole)/2+2*wireDia; // coil width
x=0;

rad=0.2; // radius of the wire bending at the core edges


offy=-(coreHole+wireDia);
offz=-z/2;
offx=-turns*spacing/2;

If (wireCrossectionSelect==1)

// wire with rectangular cross-section
Point(10000)={wireDia/2+offx,rad+offy,wireDia/2+offz,0.5};
Point(10001)={-wireDia/2+offx,rad+offy,wireDia/2+offz,0.5};
Point(10002)={-wireDia/2+offx,rad+offy,-wireDia/2+offz,0.5};
Point(10003)={wireDia/2+offx,rad+offy,-wireDia/2+offz,0.5};
Line(10000)={10000,10001};
Line(10001)={10001,10002};
Line(10002)={10002,10003};
Line(10003)={10003,10000};
Curve Loop(1) = {10000, 10001, 10002, 10003};
Plane Surface(1) = {1};


Else

Point(10004)={offx,rad+offy,offz,0.5};
Circle(10005) = {offx, rad+offy, offz, wireDia/2, 0, 2*Pi};
Rotate {{1, 0, 0},  {offx, rad+offy, offz}, Pi/2} {
  Curve{10005}; 
}
Curve Loop(1) = {10005};
Plane Surface(1) = {1};


EndIf



// 3D path for path extrude - pipe

For t In {0:(turns-1)}

//Printf("loop %g",t);

Point(12*t+1)={x+offx,rad+offy,0+offz,0.5};

Point(12*t+2)={x+offx,y-rad+offy,+offz,0.5};
Point(12*t+3)={x+offx,y-rad+offy,rad+offz,0.5};
Point(12*t+4)={x+offx,y+offy,rad+offz,0.5};

Point(12*t+5)={x+offx,y+offy,z-rad+offz,0.5};
Point(12*t+6)={x+offx,y+offy-rad,z-rad+offz,0.5};
//Point(12*t+7)={x+offx,y+offy-rad,z+offz,0.5};
//Point(12*t+7)={x+offx,y+offy-rad,z+offz,0.5};


Circle(10*t+5) = {x+offx+tanrad, y+offy-rad, z+offz, tanrad, Pi, ang1};
//Printf("ada %g",y-2*rad);

p1s() = Point In BoundingBox{x+offx-eps, y+offy-rad-eps, z+offz-eps,x+offx+eps, y+offy-rad+eps, z+offz+eps};
p1e() = Point In BoundingBox{x+offx-eps+p1x, y+offy-rad-eps+p1y, z+offz-eps,x+offx+eps+p1x, y+offy-rad+eps+p1y, z+offz+eps};

//Printf('s_%g',p1s[0]);
//Printf('e_%g',p1e[0]);

x += spacing;

Circle(10*t+7) = {x+offx-tanrad, 0+rad+offy, z+offz, tanrad, 0, ang2};

p2s() = Point In BoundingBox{x+offx-eps, +offy+rad-eps, z+offz-eps,x+offx+eps, +offy+rad+eps, z+offz+eps};
p2e() = Point In BoundingBox{x+offx-eps+p2x, +offy+rad-eps+p2y, z+offz-eps,x+offx+eps+p2x, +offy+rad+eps+p2y, z+offz+eps};
//Printf('s_%g',p2s[0]);
//Printf('e_%g',p2e[0]);


Point(12*t+9)={x+offx,0+rad+offy,z-rad+offz,0.5};
Point(12*t+10)={x+offx,0+offy,z-rad+offz,0.5};

Point(12*t+11)={x+offx,0+offy,rad+offz,0.5};
Point(12*t+12)={x+offx,rad+offy,rad+offz,0.5};

Line(10*t+1)={12*t+1,12*t+2};
Circle(10*t+2)={12*t+2,12*t+3,12*t+4};
Line(10*t+3)={12*t+4,12*t+5};
Circle(10*t+4)={12*t+5,12*t+6,p1s};
Line(10*t+6)={p1e,p2e};
Circle(10*t+8)={p2s,12*t+9,12*t+10};
Line(10*t+9)={12*t+10,12*t+11};

If (t>=1)
Circle(10*(t-1)+10)={12*(t-1)+11,12*(t-1)+12,12*(t)+1};
EndIf

EndFor

//Printf("final section");
Point(12*t+1)={x+offx,rad+offy,0+offz,0.5};

//Printf("%g %g %g",12*(t-1)+11,12*(t-1)+12,12*(t)+1);

Circle(10*(t-1)+10)={12*(t-1)+11,12*(t-1)+12,12*(t)+1};



offsetPoint=0;

// curved part of the race track winding
angle=0;

foo=0;

For t In {(turns):(turns+circleTurns-1)}


//Printf("Curved area loop %g",Cos(angle));
//Printf("point %g",12*t+1);

Point(12*t+1)={x+offx,rad+offy,0+offz,0.5};

Point(12*t+2)={x+offx,y-rad+offy,+offz,0.5};
Point(12*t+3)={x+offx,y-rad+offy,rad+offz,0.5};
Point(12*t+4)={x+offx,y+offy,rad+offz,0.5};

Point(12*t+5)={x+offx,y+offy,z-rad+offz,0.5};
Point(12*t+6)={x+offx,y+offy-rad,z-rad+offz,0.5};

Circle(10*t+5) = {x+offx+tanrad, y+offy-rad, z+offz, tanrad, Pi, ang3-0*Pi/circleTurns};
//Printf("ada %g",y-2*rad);

p3s() = Point In BoundingBox{x+offx-eps, y+offy-rad-eps, z+offz-eps,x+offx+eps, y+offy-rad+eps, z+offz+eps};
//p3e() = Point In BoundingBox{x+offx-eps+p3x, y+offy-rad-eps+p3y, z+offz-eps,x+offx+eps+p3x, y+offy-rad+eps+p3y, z+offz+eps};
p3e[0]=p3s[0]+1;

//Printf('s_%g',p3s[0]);
//Printf('e_%g',p3e[0]);

//x += spacing;

shiftX=0*shiftRAWY;

shiftY=0*shiftRAWX;

Circle(10*t+7) = {x+offx-tanrad+shiftX, 0+rad+offy+shiftY, z+offz, tanrad, 0, ang4-1*Pi/circleTurns};

//p4s() = Point In BoundingBox{x+offx-eps+shiftX, +offy+rad-eps+shiftY, z+offz-eps,x+offx+eps+shiftX, +offy+rad+eps+shiftY, z+offz+eps};
//p4e() = Point In BoundingBox{x+offx-eps+p4x+shiftX, +offy+rad-eps+p4y+shiftY, z+offz-eps,x+offx+eps+p4x+shiftX, +offy+rad+eps+p4y+shiftY, z+offz+eps};
p4e[0]=p3e[0]+1;
p4s[0]=p4e[0]+1;
//Printf('s_%g',p4s[0]);
//Printf('e_%g',p4e[0]);


//Point(12*t+8)={x+offx,0+rad+offy,z+offz,0.5};
Point(12*t+9)={x+offx,0+rad+offy,z-rad+offz,0.5};
Point(12*t+10)={x+offx,0+offy,z-rad+offz,0.5};

Point(12*t+11)={x+offx,0+offy,rad+offz,0.5};
Point(12*t+12)={x+offx,rad+offy,rad+offz,0.5};

/*Delete {
  Curve{10*t+5}; Curve{10*t+7}; 
}*/

angle += 0;
Rotate {{0, 0, 1}, {straighSectionLength/2, 0, 0}, angle} {
  Point{12*t+1}; Point{12*t+2}; Point{12*t+3}; Point{12*t+4}; Point{12*t+5}; Point{12*t+6}; Curve{10*t+5};
}
angle += Pi/circleTurns;


Rotate {{0, 0, 1}, {straighSectionLength/2, 0, 0}, angle} {
  Point{12*t+9}; Point{12*t+10}; Point{12*t+11}; Point{12*t+12};
}

Rotate {{0, 0, 1}, {straighSectionLength/2, 0, 0}, angle} {
  Curve{10*t+7};
}

Line(10*t+1+offsetPoint)={12*t+1,12*t+2};
Circle(10*t+2+offsetPoint)={12*t+2,12*t+3,12*t+4};
Line(10*t+3+offsetPoint)={12*t+4,12*t+5};
Circle(10*t+4+offsetPoint)={12*t+5,12*t+6,p1e[0]+9+foo};


Line(10*t+6+offsetPoint)={p1e[0]+8+foo,p1e[0]+10+foo};
Circle(10*t+8+offsetPoint)={p1e[0]+11+foo,12*t+9,12*t+10};
Line(10*t+9+offsetPoint)={12*t+10,12*t+11};

//Printf("circle");
foo=foo+8;

If (t>(turns))
Circle(10*(t-1)+10+offsetPoint)={12*(t-1)+11,12*(t-1)+12,12*(t)+1};
EndIf

EndFor




//x=0;
For t In {(turns+circleTurns):(2*turns+circleTurns-1)}

//Printf("loop %g",t);

Point(12*t+1)={x+offx,-rad-offy,0+offz,0.5};

Point(12*t+2)={x+offx,-y+rad-offy,+offz,0.5};
Point(12*t+3)={x+offx,-y+rad-offy,rad+offz,0.5};
Point(12*t+4)={x+offx,-y-offy,rad+offz,0.5};

Point(12*t+5)={x+offx,-y-offy,z-rad+offz,0.5};
Point(12*t+6)={x+offx,-y-offy+rad,z-rad+offz,0.5};


Circle(10*t+5) = {x+offx-tanrad, -y-offy+rad, z+offz, tanrad, 0, Pi+ang1};
//Printf("ada %g",y-2*rad);

p1s() = Point In BoundingBox{x+offx-eps, -y-offy+rad-eps, z+offz-eps,x+offx+eps, -y-offy+rad+eps, z+offz+eps};
p1e() = Point In BoundingBox{x+offx-eps+p1x, -y-offy+rad-eps-p1y, z+offz-eps,x+offx+eps+p1x, -y-offy+rad+eps-p1y, z+offz+eps};

//Printf('s_%g',p1s[0]);
//Printf('e_%g',p1e[0]);

x -= spacing;

Circle(10*t+7) = {x+offx+tanrad, 0-rad-offy, z+offz, tanrad, Pi, Pi+ang2};

p2s() = Point In BoundingBox{x+offx-eps, -offy-rad-eps, z+offz-eps,x+offx+eps, -offy-rad+eps, z+offz+eps};
p2e() = Point In BoundingBox{x+offx-eps+p2x, -offy-rad-eps-p2y, z+offz-eps,x+offx+eps+p2x, -offy-rad+eps-p2y, z+offz+eps};

//Printf('s_%g',p2s[0]);
//Printf('e_%g',p2e[0]);


Point(12*t+9)={x+offx,0-rad-offy,z-rad+offz,0.5};
Point(12*t+10)={x+offx,0-offy,z-rad+offz,0.5};

Point(12*t+11)={x+offx,0-offy,rad+offz,0.5};
Point(12*t+12)={x+offx,-rad-offy,rad+offz,0.5};

Line(10*t+1)={12*t+1,12*t+2};
Circle(10*t+2)={12*t+2,12*t+3,12*t+4};
Line(10*t+3)={12*t+4,12*t+5};
Circle(10*t+4)={12*t+5,12*t+6,p1s};
Line(10*t+6)={p1e,p2e};
Circle(10*t+8)={p2s,12*t+9,12*t+10};
Line(10*t+9)={12*t+10,12*t+11};

If (t>=1)
Circle(10*(t-1)+10)={12*(t-1)+11,12*(t-1)+12,12*(t)+1};
EndIf

EndFor

//Printf("final section");
Point(12*t+1)={x+offx,-rad-offy,0+offz,0.5};

//Printf("%g %g %g",12*(t-1)+11,12*(t-1)+12,12*(t)+1);

Circle(10*(t-1)+10)={12*(t-1)+11,12*(t-1)+12,12*(t)+1};


foo=0;
angle=0;
For t In {(2*turns+circleTurns):(2*(turns+circleTurns)-1)}

//Printf("Curved area loop %g",Cos(angle));
//Printf("point %g",12*t+1);

Point(12*t+1)={x+offx,-rad-offy,0+offz,0.5};

Point(12*t+2)={x+offx,-y+rad-offy,+offz,0.5};
Point(12*t+3)={x+offx,-y+rad-offy,rad+offz,0.5};
Point(12*t+4)={x+offx,-y-offy,rad+offz,0.5};

Point(12*t+5)={x+offx,-y-offy,z-rad+offz,0.5};
Point(12*t+6)={x+offx,-y-offy+rad,z-rad+offz,0.5};

Circle(10*t+5) = {x+offx-tanrad, -y-offy+rad, z+offz, tanrad, 0, ang3-Pi-0*Pi/circleTurns};
//Printf("ada %g",y-2*rad);

p3s() = Point In BoundingBox{x+offx-eps, -y-offy+rad-eps, z+offz-eps,x+offx+eps, -y-offy+rad+eps, z+offz+eps};
//p3e() = Point In BoundingBox{x+offx-eps+p3x, -y-offy-rad-eps-p3y, z+offz-eps,x+offx+eps+p3x, -y-offy-rad+eps-p3y, z+offz+eps};
p3e[0]=p3s[0]+1;

//Printf('s_%g',p3s[0]);
//Printf('e_%g',p3e[0]);

//x += spacing;

shiftX=0*shiftRAWY;

shiftY=0*shiftRAWX;

Circle(10*t+7) = {x+offx+tanrad+shiftX, 0-rad-offy+shiftY, z+offz, tanrad, Pi, Pi+ang4-Pi/circleTurns};

//p4s() = Point In BoundingBox{x+offx-eps+shiftX, -offy+rad-eps+shiftY, z+offz-eps,x+offx+eps+shiftX, -offy+rad+eps+shiftY, z+offz+eps};
//p4e() = Point In BoundingBox{x+offx-eps+p4x+shiftX, -offy+rad-eps+p4y+shiftY, z+offz-eps,x+offx+eps+p4x+shiftX, -offy+rad+eps+p4y+shiftY, z+offz+eps};
p4e[0]=p3e[0]+1;
p4s[0]=p4e[0]+1;
//Printf('s_%g',p4s[0]);
//Printf('e_%g',p4e[0]);


//Point(12*t+8)={x+offx,0+rad+offy,z+offz,0.5};
Point(12*t+9)={x+offx,0-rad-offy,z-rad+offz,0.5};
Point(12*t+10)={x+offx,0-offy,z-rad+offz,0.5};

Point(12*t+11)={x+offx,0-offy,rad+offz,0.5};
Point(12*t+12)={x+offx,-rad-offy,rad+offz,0.5};


/*Delete {
  Curve{10*t+5}; Curve{10*t+7}; 
}*/

angle += 0;
Rotate {{0, 0, 1}, {-straighSectionLength/2, 0, 0}, angle} {
  Point{12*t+1}; Point{12*t+2}; Point{12*t+3}; Point{12*t+4}; Point{12*t+5}; Point{12*t+6}; Curve{10*t+5};
}
angle += Pi/circleTurns;


Rotate {{0, 0, 1}, {-straighSectionLength/2, 0, 0}, angle} {
  Point{12*t+9}; Point{12*t+10}; Point{12*t+11}; Point{12*t+12};
}

Rotate {{0, 0, 1}, {-straighSectionLength/2, 0, 0}, angle} {
  Curve{10*t+7};
}

Line(10*t+1+offsetPoint)={12*t+1,12*t+2};
Circle(10*t+2+offsetPoint)={12*t+2,12*t+3,12*t+4};
Line(10*t+3+offsetPoint)={12*t+4,12*t+5};
Circle(10*t+4+offsetPoint)={12*t+5,12*t+6,p1e[0]+9+foo};


Line(10*t+6+offsetPoint)={p1e[0]+8+foo,p1e[0]+10+foo};
Circle(10*t+8+offsetPoint)={p1e[0]+11+foo,12*t+9,12*t+10};
Line(10*t+9+offsetPoint)={12*t+10,12*t+11};

//Printf("circle");
foo=foo+8;

If (t>(turns))
Circle(10*(t-1)+10+offsetPoint)={12*(t-1)+11,12*(t-1)+12,12*(t)+1};
EndIf

EndFor


Recursive Delete {
  Point{t*12-2}; Curve{t*10-1}; Point{t*12-1}; Curve{t*10-2}; Point{t*12-3}; Point{t*12}; 
}


// Pipe wire extrude
Wire(30) = {1:(10*(2*turns)+2*circleTurns*10-3)};

Extrude { Surface{1}; } Using Wire {30}

// creates physical surfaces for connection to Inductex ports
Physical Surface("excP1", 20000) = {1};

sid=Surface In BoundingBox{offx-wireDia, rad+offy-eps, -offz-wireDia,offx+wireDia, rad+offy+eps, -offz+wireDia};
pid=Surface In BoundingBox{offx-wireDia, rad+offy-eps, -offz-wireDia,offx+wireDia, rad+offy+eps, -offz+wireDia};
//Printf("sid %g pid %g",  sid[0],pid[0]);

Physical Surface("excP2", 20001) = {sid[0]};

// Permalloy core model - disable 

// racetrack layout for debug only - core volume is modeled in core_opencascade.geo

If (0)

//+
Point(40001) = {-straighSectionLength/2 , sectionWidth/2, -coreHeight/2, 1.0};
//+
Point(40002) = {-straighSectionLength/2 , -sectionWidth/2, -coreHeight/2, 1.0};
//+
Point(40003) = {straighSectionLength/2 , sectionWidth/2, -coreHeight/2, 1.0};
//+
Point(40004) = {straighSectionLength/2 , -sectionWidth/2, -coreHeight/2, 1.0};
//+
Point(40005) = {straighSectionLength/2 , coreHole/2, -coreHeight/2, 1.0};
//+
Point(40006) = {straighSectionLength/2 , -coreHole/2, -coreHeight/2, 1.0};
//+
Point(40007) = {-straighSectionLength/2 , coreHole/2, -coreHeight/2, 1.0};
//+
Point(40008) = {-straighSectionLength/2 , -coreHole/2, -coreHeight/2, 1.0};
//+
Point(40009) = {straighSectionLength/2 , 0, -coreHeight/2, 1.0};
//+
Point(40010) = {-straighSectionLength/2 , 0, -coreHeight/2, 1.0};


//+
Circle(40001) = {40002, 40010, 40001};
//+
Circle(40002) = {40008, 40010, 40007};
//+
Circle(40003) = {40003, 40009, 40004};
//+
Circle(40004) = {40005, 40009, 40006};
//+
Line(40005) = {40004, 40002};
//+
Line(40006) = {40006, 40008};
//+
Line(40007) = {40005, 40007};

Line(40008) = {40003, 40001};

EndIf
